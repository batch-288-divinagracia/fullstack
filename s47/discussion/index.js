// console.log(`hello`)

// Section: Document Object Model (DOM)
	// Allows us to access or modify the properties of an html elements in a webpage
	// it is standard on how to get, change, add or delete html elements
	// we will be focusing only with DOM in terms of managing forms

	// using the querySelector it can access/get the HTML elements/s

	// CSS selectors to target specific elements
		/*
		id selector(#);
		class selector(.);
		tag/type selector (html tags);
		universal selector(*);
		attribute selector([attribute]);
		*/

	// Query selector has two types: querySelector and querySelectorAll
// let firstElement = document.querySelector('#txt-first-name');
// console.log(firstElement);

	// querySelector
	let secondElement = document.querySelector('.full-name');
	console.log(secondElement);
	// querySelectorAll
	let thirdElement = document.querySelectorAll('.full-name');
	console.log(thirdElement);

	// Get elements 
	let element = document.getElementById("fullName");
	console.log(element);

	element = document.getElementsByClassName("full-name")
	console.log(element)

// Section: event listeners
	//  whenever a user interacts with a webpage, this action is considered as event 
	// working with events is large part of creating interactivity in a webpage
	//  specific function will be invoked if the event happen.

	// function 'addEvenetListener`, it takes two arguments
		// firts argument a string identifiying the event
		// second, argument function that the listener will invoke once the specified even occur

	let fullName = document.querySelector("#fullName");

	let firstElement = document.querySelector("#txt-first-name");

	let txtLastName = document.querySelector("#txt-last-name");

	firstElement.addEventListener('keyup', () => {
		console.log(firstElement.value);

		fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`;
	})

	
	txtLastName.addEventListener(`keyup`, () => {
		console.log(txtLastName.value);

		fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`;
	})

function changeColor(event){
	let color = event.value;
	document.querySelector('#fullName')
	fullName.style.color=color;
 }
