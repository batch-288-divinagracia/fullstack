import React from 'react';

// Create a context Object 
// A context Object as the name states is adata that can be used to store information that can be shared to other component/s within the app.

// the context object is different approach to passing information between components and allows easir access by avoiding to use of prop passing.


// with the help of createContext() method we were able to create a context stored in a variable UserContext.
const UserContext = React.createContext();


// the provider component allows other components to consume/use the context object and supply necessary information needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;