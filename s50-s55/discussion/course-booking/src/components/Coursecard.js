import {Container, Row, Col, Button, Card} from 'react-bootstrap';

// import the useState hook from react
import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

import {Link} from 'react-router-dom';

export	default function Coursecard(props){
	// consume the content of the UserContext
	const {user} = useContext(UserContext);

	console.log(props.courseProp)

	// object distructuring
	const {_id, name, description, price} = props.courseProp;

	// Syntax: const [getter, setter] = useState (initialGetterValue);

	
	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(30);
	const [isDisabled, setIsDisabled] = useState(false);

	// this function will be 
	function enroll(){
		if(seat !== 0){
			setCount(count+1);
			setSeat(seat-1);


		} else {
			alert('No more Seats Available!');
		}
	}

	//The function or the side effect in our useEffect hook will invoke or run on the initial loading of our application and when there is /are change or changes on our dependencies.
	// syntax:
		// useEffect(side effect, [dependecies]);
	useEffect(() => {

		if(seat === 0){
			// it will the state or value of isDisabled is true
			setIsDisabled(true)
		}

	}, [seat]); 

	return(
		<Container className= "mt-5">
			<Row>
				<Col className= "col-12 mt-3">
					<Card>
					      <Card.Body>
					        <Card.Title className= "fs-2">{name}</Card.Title>
					        <h4>Description</h4>
					        <Card.Text>
					          {description}
					        </Card.Text>
					        <h4>Price</h4>
					        <Card.Text>
					          {price}
					        </Card.Text>
					        <Card.Subtitle>Enrollees:</Card.Subtitle>
					        <Card.Text>{count}</Card.Text>
					        {
					        	user !== null 
					        	?
					        	<Button as = {Link} to = {`/courses/${_id}`}>Details</Button>
					        	:
					        	<Button as = {Link} to = '/login'>Login to Enroll!</Button>
					        }

					        
					      </Card.Body>
					    </Card>
				</Col>
			</Row>
		</Container>


		)
}