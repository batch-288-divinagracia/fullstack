import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
// we use link and navlink to add hyperlink  to our application, we Navlink for navigation bars
import {Link, NavLink} from 'react-router-dom';
import {useContext} from 'react';

import UserContext from '../UserContext';

// the 'as' allows components to be treated as if they are a different components gaining the access to its properties and functionalities
export default function AppNavbar(){

	// const [user, setUser] = useState(localStorage.getItem('email'));

	// we will consume the user state from the global context

	const {user} = useContext(UserContext);

	return(
		<>
		     <Navbar bg="dark" variant="dark">
		       <Container>
		         <Navbar.Brand as = {Link} to = '/'>Zuitt</Navbar.Brand>
		         <Nav className="ms-auto">
		           <Nav.Link as = {NavLink} to = '/' >Home</Nav.Link>
		           <Nav.Link as = {NavLink} to = '/courses'>Courses</Nav.Link>
		           {
		           		user.id === null

		           		?

		           		<>
		           			<Nav.Link as = {NavLink} to ='/register'>Register</Nav.Link>
		           			<Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
		           		</>

		           		:

		           		<Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
		           }
		         </Nav>
		       </Container>
		     </Navbar>
		   </>
		)
}