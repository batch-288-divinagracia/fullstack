import coursesData from '../data/Courses.js'
import Coursecard from '../components/Coursecard.js'
import {useState, useEffect} from 'react'
export default function Courses(){
	//we console the coursesdata to check whether we imported the mock database properly
	// console.log(coursesData);

	// the prop that will be passed will be the object data and the name of the prop will be the name of the attribute when passed.

	// const courses = coursesData.map(course => {

	// 	return(
	// 		<Coursecard key = {course.id} courseProp = {course}/>

	// 		)
	// })

	const [courses, setCourses] = useState('');
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/activeCourses`)
		.then(result => result.json())
		.then(data => {
			// console.log(data)
			setCourses(data.map(course =>{
				return(
					<Coursecard key = {course._id} courseProp = {course}/>
				)
			}))
		})
	}, [])



	return(
		<>
		<h1 className = 'text-center mt-3'>Courses</h1>
		{courses}
		</>

		)
}