import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

import {Link, Navigate, useNavigate} from 'react-router-dom';

import Swal2 from 'sweetalert2';



export default function Register(){

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [isDisabled, setIsDisabled] =useState(true);
	// we consume the setUser function from the UserContext.
	const {user, setUser} = useContext(UserContext);

	// we contain the useNavigate to navigate variable
	const navigate = useNavigate();

	// we have to use useEffect in enabling the submit button
	useEffect(() => {
		if(email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && firstName !== '' && lastName !== '' && mobileNo !== '' && mobileNo.length === 11){

			setIsDisabled(false);
		} else {

			setIsDisabled(true);
		}

	}, [email, password1, password2, firstName, lastName, mobileNo])

	// function that will triggered once we submit the form
	function register(event){
		// it prevents our pages to reload when submitting the form
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				email: email,
				password: password1
			

			})

		})
		.then(result => result.json())
		.then(data => {
			if(!data){
				console.log(data)
				Swal2.fire({
					title: 'Error',
					icon: 'error',
					text: 'Please try again'

				})
			} else {
				console.log(data)
				Swal2.fire({
					title: 'Registered Successfully',
					icon: 'success',
					text: 'Please Log in'

				})
				navigate('/login')

			}

		})

		// localStorage.setItem('email', email);
		// setUser(localStorage.getItem('email'));

		// alert('Thank you for registering')

		// navigate('/')

		//clear input fields
		// setEmail('');
		// setPassword1('');
		// setPassword2('');

	}

	

	


	
	return(
		(user.id === null) ?
		<Container className = 'mt-5'>
			<Row>
				<Col className = 'col-6  mx-auto'>
					<h1 className = 'text-center'>Register</h1>
					<Form onSubmit = {event => register(event)}>
					      
						 <Form.Group className="mb-3" controlId="formBasicFirstName">
						   <Form.Label>First Name</Form.Label>
						   <Form.Control type="String" value = {firstName} onChange = {event => setFirstName(event.target.value)} placeholder="Enter First Name" />
						 </Form.Group>
						 <Form.Group className="mb-3" controlId="formBasicLasttName">
						   <Form.Label>Last Name</Form.Label>
						   <Form.Control type="String" value = {lastName} onChange = {event => setLastName(event.target.value)} placeholder="Enter Last Name" />
						 </Form.Group>

						 <Form.Group className="mb-3" controlId="formBasicMobileNo">
						   <Form.Label>Mobile No.</Form.Label>
						   <Form.Control type="Number" value = {mobileNo} onChange = {event => setMobileNo(event.target.value)} placeholder="Enter Mobile No." />
						 </Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control type="email" value = {email} onChange = {event => setEmail(event.target.value)} placeholder="Enter email" />
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicPassword1">
					        <Form.Label>Password</Form.Label>
					        <Form.Control type="password" value = {password1} onChange = {event => setPassword1(event.target.value)} placeholder="Password" />
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicPassword2">
					        <Form.Label>Verify Password</Form.Label>
					        <Form.Control type="password" value = {password2} onChange = {event => setPassword2(event.target.value)} placeholder="Password" />
					      </Form.Group>
					      
					      <p>Have an account? <Link to = '/login'>login in here</Link></p>
					      
					      <Button variant="primary" type="submit"  disabled = {isDisabled}>
					        Submit
					      </Button>
					    </Form>					
				</Col>	
			</Row>
		</Container>
		:
		<Navigate to = '/PageNotFound' />

		)
}