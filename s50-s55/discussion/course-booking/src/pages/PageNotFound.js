import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import pagenotfound from '../images/pagenotfound.jpg';


export default function PageNotFound(){

	return(
		<Container className= 'mt-5'>
			<Row>
				<Col className = "col-6 mx-auto">
					<div>
						<h1>Page Not Found</h1>
						<img className= ""src = {pagenotfound} alt="Page Not Found"/>
						<h4>Go back to the <Link to = '/'>homepage</Link>.</h4>						
					</div>
				</Col>
			</Row>
		</Container>
	)
}
