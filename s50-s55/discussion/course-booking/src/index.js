import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// createRoot() - assigns the element to be manage by REact with its vertual DOM
const root = ReactDOM.createRoot(document.getElementById('root'));

// render() -displaying the component/react element into the root
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


